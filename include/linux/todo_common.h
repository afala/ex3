/* todo_common.c */

/*************************************************************************/

// Includes:

#include <linux/kernel.h>  	
#include <linux/fs.h>       		
#include <asm/uaccess.h>
#include <linux/errno.h> 
#include <linux/slab.h> 
#include <asm/current.h>
#include <linux/list.h>
#include <linux/sched.h>
#include <linux/types.h>
#include <linux/time.h>

/*************************************************************************/

// Constants:

#define SUCCESS 0
#define FAILURE -1

typedef enum {
    FALSE,
    TRUE,
} BOOL;

/*************************************************************************/

// Structs

typedef struct TODO {
	int status;
	char* description;
	int description_size;
	struct list_head queue_list;
    time_t deadline;
    time_t deadline_relative;
} TODO;

// Function Declarations:

int TODO_COMMON_isLegalTask(task_t *p_current, pid_t pid, task_t **pp_target_task_struct);
