/* add_TODO.h */

#ifndef __LINUX_ADD_TODO_H__
#define __LINUX_ADD_TODO_H__

#include <linux/todo_common.h>

asmlinkage int sys_add_TODO(pid_t pid, const char* TODO_description, ssize_t description_size, time_t TODO_deadline);
extern int TODO_COMMON_isLegalTask(task_t *p_current, pid_t pid, task_t **pp_target_task_struct);

#endif //__LINUX_ADD_TODO_H__
