/* read_TODO.h */

#ifndef __LINUX_READ_TODO_H__
#define __LINUX_READ_TODO_H__

#include <linux/todo_common.h>

asmlinkage ssize_t sys_read_TODO(pid_t pid, int TODO_index, char* TODO_description, time_t *TODO_deadline, int* status);
extern int TODO_COMMON_isLegalTask(task_t *p_current, pid_t pid, task_t **pp_target_task_struct);

#endif //__LINUX_READ_TODO_H__