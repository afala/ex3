/* delete_TODO.h */

#ifndef __LINUX_DELETE_TODO_H__
#define __LINUX_DELETE_TODO_H__

#include <linux/todo_common.h>

asmlinkage int sys_delete_TODO(pid_t pid, int TODO_index);
extern int TODO_COMMON_isLegalTask(task_t *p_current, pid_t pid, task_t **pp_target_task_struct);

#endif //__LINUX_DELETE_TODO_H__