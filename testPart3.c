#include "todo_api.h"
#include <stdio.h>

#include <linux/module.h>  /* Needed by all modules */
#include <linux/kernel.h>  /* Needed for KERN_ALERT */
#include <linux/list.h>  /* Needed by all modules */
#include <linux/types.h>  /* Needed for KERN_ALERT */
#include <linux/unistd.h>  /* Needed for KERN_ALERT */
#include <sys/types.h>
#include <sys/wait.h>
#include <time.h>

#include <limits.h>

//C hello world example
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>     /* malloc, free, rand */


#define LATE_TODO_PENALTY 60



void checkResult( int returnValue , int expectedReturnValue, int expectedError, const char* testName ) {
	
	int errsv = errno;
	if (returnValue != expectedReturnValue) {
		printf("checkResult failed, wrong return value - %s\n", testName);
	}

	if (returnValue == expectedReturnValue && returnValue == -1) {
		if (errsv != expectedError) {
			printf("checkResult failed, wrong error value - %s\n", testName);
		}
	}

	
}

void checkStatus (int one, int two,const char* testName) {
	if (one != two) {
		printf("Check number %s failed, wrong returned status\n", testName);
	}
}

void checkDeadline (time_t one, time_t two,const char* testName) {
	if (one != two) {
		printf("checkDeadline %s failed, wrong returned deadline\n", testName);
	}
}

void checkDesc (char* one, char* two, const char* testName) {
	if (strcmp(one, two) != 0) {
		printf("checkDesc failed - %s\n", testName);
	}
}


void checkDelay (time_t start, time_t finish, unsigned int numberOfDelayedTODOs, const char* testName) {
	if (start + numberOfDelayedTODOs*LATE_TODO_PENALTY > finish) {
		printf("checkDelay failed - %s\n", testName);
	}
}



void checkAddOrder () {
	int sizeOfReadBuffer = 80;
	int descLength = 13;
	printf("--- checkAddOrder ---\n");
	pid_t myPid = getpid();
	int* pStatus = (int*) malloc (sizeof(int));
	char* pDesc = (char*) malloc (sizeof(char)*sizeOfReadBuffer);	
	time_t* pDeadline = (time_t*) malloc (sizeof(time_t));	
	
	char str1[40] = "Description_1";
	char str2[40] = "Description_2";
	char str3[40] = "Description_3";
	char str4[40] = "Description_4";
	char str5[40] = "Description_5";
	char str6[40] = "Description_6";

	
	time_t time1 = time(NULL) + 3;
	time_t time2 = time(NULL) + 6;
	time_t time3 = time(NULL) + 4;
	time_t time4 = time(NULL) + 2;
	time_t time5 = time(NULL) + 1;
	time_t time6 = time(NULL) + 5;
	
	

	// CHECK ALL FUNCTIONALITY, SINGLE PROCESS
    


	checkResult(add_TODO(myPid, str1, descLength,time1) , 0 , 0, "add1");
    printf("add1 passed!\n");
	checkResult(add_TODO(myPid, str2, descLength,time2) , 0 , 0, "add2");	
    printf("add2 passed!\n");
	checkResult(add_TODO(myPid, str3, descLength,time3) , 0 , 0, "add3");		
	checkResult(add_TODO(myPid, str4, descLength,time4) , 0 , 0, "add4");		
	checkResult(add_TODO(myPid, str5, descLength,time5) , 0 , 0, "add5");		
	checkResult(add_TODO(myPid, str6, descLength,time6) , 0 , 0, "add6");		
	
	
	// expected todo queue is :
	// 5 -> 4 -> 1 -> 3 -> 6 -> 2
	
	
	checkResult(read_TODO(myPid, 1, pDesc,pDeadline, pStatus) , descLength , 0, "check 1st element");	
	checkStatus (*pStatus, 0,"check 1st element");
	checkDeadline (*pDeadline, time5,"check 1st element");
	checkDesc (pDesc, str5, "check 1st element");

	checkResult(read_TODO(myPid, 2, pDesc,pDeadline, pStatus) , descLength , 0, "check 2nd element");	
	checkStatus (*pStatus, 0, "check 2nd element");
	checkDeadline (*pDeadline, time4, "check 2nd element");
	checkDesc (pDesc, str4, "check 2nd element");	
	
	checkResult(read_TODO(myPid, 3, pDesc,pDeadline, pStatus) , descLength , 0, "check 3rd element");	
	checkStatus (*pStatus, 0, "check 3rd element");
	checkDeadline (*pDeadline, time1, "check 3rd element");
	checkDesc (pDesc, str1, "check 3rd element");		
	
	checkResult(read_TODO(myPid, 4, pDesc,pDeadline, pStatus) , descLength , 0, "check 4th element");	
	checkStatus (*pStatus, 0, "check 4th element");
	checkDeadline (*pDeadline, time3, "check 4th element");
	checkDesc (pDesc, str3, "check 4th element");		

	checkResult(read_TODO(myPid, 5, pDesc,pDeadline, pStatus) , descLength , 0, "check 5th element");	
	checkStatus (*pStatus, 0, "check 5th element");
	checkDeadline (*pDeadline, time6, "check 5th element");
	checkDesc (pDesc, str6, "check 5th element");	
	
	checkResult(read_TODO(myPid, 6, pDesc,pDeadline, pStatus) , descLength , 0, "check 6th element");	
	checkStatus (*pStatus, 0, "check 6th element");
	checkDeadline (*pDeadline, time2, "check 6th element");
	checkDesc (pDesc, str2, "check 6th element");		

	
	checkResult(delete_TODO(myPid, 6) , 0 , 0, "delete 6th item");	
	checkResult(delete_TODO(myPid, 5) , 0 , 0, "delete 5th item");	
	checkResult(delete_TODO(myPid, 4) , 0 , 0, "delete 4th item");	
	checkResult(delete_TODO(myPid, 3) , 0 , 0, "delete 3rd item");	
	checkResult(delete_TODO(myPid, 2) , 0 , 0, "delete 2nd item");	
	checkResult(delete_TODO(myPid, 1) , 0 , 0, "delete 1st item");	

	free(pStatus);
	free(pDesc);
	free(pDeadline);


}



void checkRemovalAndDelayOfExpiredTODOsTestNumber1 () {
	int sizeOfReadBuffer = 80;
	int descLength = 13;
	printf("--- checkRemovalAndDelayOfExpiredTODOsTestNumber1 ---\n");
	pid_t myPid = getpid();
	unsigned long i = 0;
	int* pStatus = (int*) malloc (sizeof(int));
	char* pDesc = (char*) malloc (sizeof(char)*sizeOfReadBuffer);	
	time_t* pDeadline = (time_t*) malloc (sizeof(time_t));	
	
	char str1[40] = "Description_1";
	char str2[40] = "Description_2";
	char str3[40] = "Description_3";
	char str4[40] = "Description_4";
	char str5[40] = "Description_5";
	char str6[40] = "Description_6";


	time_t start_time = time(NULL);
	time_t finish_time = time(NULL);
	
	time_t time1 = start_time + 3000;
	time_t time2 = start_time + 8000;
	time_t time3 = start_time + 6000;
	time_t time4 = start_time + 2000;
	time_t time5 = start_time + 1;
	time_t time6 = start_time + 7000;

	// CHECK ALL FUNCTIONALITY, SINGLE PROCESS
	checkResult(add_TODO(myPid, str1, descLength,time1) , 0 , 0, "add1");
	checkResult(add_TODO(myPid, str2, descLength,time2) , 0 , 0, "add2");	
	checkResult(add_TODO(myPid, str3, descLength,time3) , 0 , 0, "add3");		
	checkResult(add_TODO(myPid, str4, descLength,time4) , 0 , 0, "add4");		
	checkResult(add_TODO(myPid, str5, descLength,time5) , 0 , 0, "add5");		
	checkResult(add_TODO(myPid, str6, descLength,time6) , 0 , 0, "add6");		

	
	printf("current time is %lu\n",(unsigned long)time(NULL)-(unsigned long)start_time);
	// busy waiting
	i = 0;
	while (i < 300000000) {
		i++;
	}

	printf("current time is %lu\n",(unsigned long)time(NULL)-(unsigned long)start_time);
	// busy waiting
	i = 0;
	while (i < 300000000) {
		i++;
	}	


	printf("current time is %lu\n",(unsigned long)time(NULL)-(unsigned long)start_time);
	// busy waiting
	i = 0;
	while (i < 300000000) {
		i++;
	}


	printf("current time is %lu\n",(unsigned long)time(NULL)-(unsigned long)start_time);
	// busy waiting
	i = 0;
	while (i < 300000000) {
		i++;
	}


	printf("current time is %lu\n",(unsigned long)time(NULL)-(unsigned long)start_time);
	// busy waiting
	i = 0;
	while (i < 300000000) {
		i++;
	}
	
	finish_time = time(NULL);

	// expecting two todos to expire
	checkDelay(start_time, finish_time, 1, "TestNumber 1");	

	// expected todo queue is :
	// 4 -> 1 -> 3 -> 6 -> 2
	
	checkResult(read_TODO(myPid, 1, pDesc,pDeadline, pStatus) , descLength , 0, "check 1st element");	
	checkStatus (*pStatus, 0, "check 2nd element");
	checkDeadline (*pDeadline, time4, "check 2nd element");
	checkDesc (pDesc, str4, "check 2nd element");	
	
	checkResult(read_TODO(myPid, 2, pDesc,pDeadline, pStatus) , descLength , 0, "check 2nd element");	
	checkStatus (*pStatus, 0, "check 3rd element");
	checkDeadline (*pDeadline, time1, "check 3rd element");
	checkDesc (pDesc, str1, "check 3rd element");		
	
	checkResult(read_TODO(myPid, 3, pDesc,pDeadline, pStatus) , descLength , 0, "check 3rd element");	
	checkStatus (*pStatus, 0, "check 4th element");
	checkDeadline (*pDeadline, time3, "check 4th element");
	checkDesc (pDesc, str3, "check 4th element");		

	checkResult(read_TODO(myPid, 4, pDesc,pDeadline, pStatus) , descLength , 0, "check 4th element");	
	checkStatus (*pStatus, 0, "check 5th element");
	checkDeadline (*pDeadline, time6, "check 5th element");
	checkDesc (pDesc, str6, "check 5th element");	
	
	checkResult(read_TODO(myPid, 5, pDesc,pDeadline, pStatus) , descLength , 0, "check 5th element");	
	checkStatus (*pStatus, 0, "check 6th element");
	checkDeadline (*pDeadline, time2, "check 6th element");
	checkDesc (pDesc, str2, "check 6th element");		
	
	checkResult(read_TODO(myPid, 6, pDesc, pDeadline, pStatus) , -1 , EINVAL, "check 6th element");		
	
	checkResult(delete_TODO(myPid, 6) , -1 , EINVAL, "delete 6th item");	
	checkResult(delete_TODO(myPid, 5) , 0 , 0, "delete 5th item");	
	checkResult(delete_TODO(myPid, 4) , 0 , 0, "delete 4th item");	
	checkResult(delete_TODO(myPid, 3) , 0 , 0, "delete 3rd item");	
	checkResult(delete_TODO(myPid, 2) , 0 , 0, "delete 2nd item");	
	checkResult(delete_TODO(myPid, 1) , 0 , 0, "delete 1st item");	

	
	free(pStatus);
	free(pDesc);
	free(pDeadline);

	
}



void checkRemovalAndDelayOfExpiredTODOsTestNumber2 () {
	int sizeOfReadBuffer = 80;
	int descLength = 13;
	printf("--- checkRemovalAndDelayOfExpiredTODOsTestNumber2 ---\n");
	pid_t myPid = getpid();
	unsigned long i = 0;
	int* pStatus = (int*) malloc (sizeof(int));
	char* pDesc = (char*) malloc (sizeof(char)*sizeOfReadBuffer);	
	time_t* pDeadline = (time_t*) malloc (sizeof(time_t));	
	
	char str1[40] = "Description_1";
	char str2[40] = "Description_2";
	char str3[40] = "Description_3";
	char str4[40] = "Description_4";
	char str5[40] = "Description_5";
	char str6[40] = "Description_6";

	
	time_t start_time = time(NULL);
	time_t finish_time = time(NULL);
	
	time_t time1 = start_time + 3000;
	time_t time2 = start_time + 8000;
	time_t time3 = start_time + 6000;
	time_t time4 = start_time + 4;
	time_t time5 = start_time + 1;
	time_t time6 = start_time + 7000;
	
	// expected todo queue is :
	// 5 -> 4 -> 1 -> 3 -> 6 -> 2
	
		
	// add todos, different delays
	checkResult(add_TODO(myPid, str1, descLength,time1) , 0 , 0, "add1");
	checkResult(add_TODO(myPid, str2, descLength,time2) , 0 , 0, "add2");	
	checkResult(add_TODO(myPid, str3, descLength,time3) , 0 , 0, "add3");		
	checkResult(add_TODO(myPid, str4, descLength,time4) , 0 , 0, "add4");		
	checkResult(add_TODO(myPid, str5, descLength,time5) , 0 , 0, "add5");		
	checkResult(add_TODO(myPid, str6, descLength,time6) , 0 , 0, "add6");		

	printf("current time is %lu\n",(unsigned long)time(NULL)-(unsigned long)start_time);
	// busy waiting
	i = 0;
	while (i < 300000000) {
		i++;
	}

	printf("current time is %lu\n",(unsigned long)time(NULL)-(unsigned long)start_time);
	// busy waiting
	i = 0;
	while (i < 300000000) {
		i++;
	}	


	printf("current time is %lu\n",(unsigned long)time(NULL)-(unsigned long)start_time);
	// busy waiting
	i = 0;
	while (i < 300000000) {
		i++;
	}


	printf("current time is %lu\n",(unsigned long)time(NULL)-(unsigned long)start_time);
	// busy waiting
	i = 0;
	while (i < 300000000) {
		i++;
	}


	printf("current time is %lu\n",(unsigned long)time(NULL)-(unsigned long)start_time);
	// busy waiting
	i = 0;
	while (i < 300000000) {
		i++;
	}

	finish_time = time(NULL);

	
	 // printf("start time is  %d\n",(int)start_time);
	 // printf("finish time is %d\n",(int)finish_time);	
	
	// expecting two todos to expire
	checkDelay(start_time, finish_time, 2, "TestNumber 2");	
	
	
	// expected todo queue is :
	// 1 -> 3 -> 6 -> 2


	checkResult(read_TODO(myPid, 1, pDesc,pDeadline, pStatus) , descLength , 0, "check 1st element");	
	checkStatus (*pStatus, 0, "check 3rd element");
	checkDeadline (*pDeadline, time1, "check 3rd element");
	checkDesc (pDesc, str1, "check 3rd element");		
	
	checkResult(read_TODO(myPid, 2, pDesc,pDeadline, pStatus) , descLength , 0, "check 2nd element");	
	checkStatus (*pStatus, 0, "check 4th element");
	checkDeadline (*pDeadline, time3, "check 4th element");
	checkDesc (pDesc, str3, "check 4th element");		

	checkResult(read_TODO(myPid, 3, pDesc,pDeadline, pStatus) , descLength , 0, "check 3rd element");	
	checkStatus (*pStatus, 0, "check 5th element");
	checkDeadline (*pDeadline, time6, "check 5th element");
	checkDesc (pDesc, str6, "check 5th element");	
	
	checkResult(read_TODO(myPid, 4, pDesc,pDeadline, pStatus) , descLength , 0, "check 4th element");	
	checkStatus (*pStatus, 0, "check 6th element");
	checkDeadline (*pDeadline, time2, "check 6th element");
	checkDesc (pDesc, str2, "check 6th element");		

	checkResult(read_TODO(myPid, 5, pDesc, pDeadline, pStatus) , -1 , EINVAL, "check 5th element");		

	checkResult(read_TODO(myPid, 6, pDesc, pDeadline, pStatus) , -1 , EINVAL, "check 6th element");		




	
	checkResult(delete_TODO(myPid, 6) , -1 , EINVAL, "delete 6th item");	
	checkResult(delete_TODO(myPid, 5) , -1 , EINVAL, "delete 5th item");	
	checkResult(delete_TODO(myPid, 4) , 0 , 0, "delete 4th item");	
	checkResult(delete_TODO(myPid, 3) , 0 , 0, "delete 3rd item");	
	checkResult(delete_TODO(myPid, 2) , 0 , 0, "delete 2nd item");	
	checkResult(delete_TODO(myPid, 1) , 0 , 0, "delete 1st item");	

	
	free(pStatus);
	free(pDesc);
	free(pDeadline);

	
}






void checkSkippingRemovalAndDelayOfExpiredTODOsTestNumber3 () {
	int sizeOfReadBuffer = 80;
	int descLength = 13;
	printf("--- checkSkippingRemovalAndDelayOfExpiredTODOsTestNumber3 ---\n");
	pid_t myPid = getpid();
	unsigned long i = 0;
	int* pStatus = (int*) malloc (sizeof(int));
	char* pDesc = (char*) malloc (sizeof(char)*sizeOfReadBuffer);	
	time_t* pDeadline = (time_t*) malloc (sizeof(time_t));	
	
	char str1[40] = "Description_1";
	char str2[40] = "Description_2";
	char str3[40] = "Description_3";
	char str4[40] = "Description_4";
	char str5[40] = "Description_5";
	char str6[40] = "Description_6";

	
	time_t start_time = time(NULL);
	time_t finish_time = time(NULL);
	
	time_t time1 = start_time + 3000;
	time_t time2 = start_time + 8000;
	time_t time3 = start_time + 6000;
	time_t time4 = start_time + 4;
	time_t time5 = start_time + 1;
	time_t time6 = start_time + 7000;
	
	// expected todo queue is :
	// 5 -> 4 -> 1 -> 3 -> 6 -> 2

		
	// add todos, different delays
	checkResult(add_TODO(myPid, str1, descLength,time1) , 0 , 0, "add1");
	checkResult(add_TODO(myPid, str2, descLength,time2) , 0 , 0, "add2");	
	checkResult(add_TODO(myPid, str3, descLength,time3) , 0 , 0, "add3");		
	checkResult(add_TODO(myPid, str4, descLength,time4) , 0 , 0, "add4");		
	checkResult(add_TODO(myPid, str5, descLength,time5) , 0 , 0, "add5");		
	checkResult(add_TODO(myPid, str6, descLength,time6) , 0 , 0, "add6");		

	checkResult(mark_TODO(myPid, 1, 9) , 0 , 0, "mark 1st element as 9");	
	
	printf("current time is %lu\n",(unsigned long)time(NULL)-(unsigned long)start_time);
	// busy waiting
	i = 0;
	while (i < 300000000) {
		i++;
	}

	printf("current time is %lu\n",(unsigned long)time(NULL)-(unsigned long)start_time);
	// busy waiting
	i = 0;
	while (i < 300000000) {
		i++;
	}	


	printf("current time is %lu\n",(unsigned long)time(NULL)-(unsigned long)start_time);
	// busy waiting
	i = 0;
	while (i < 300000000) {
		i++;
	}


	printf("current time is %lu\n",(unsigned long)time(NULL)-(unsigned long)start_time);
	// busy waiting
	i = 0;
	while (i < 300000000) {
		i++;
	}


	printf("current time is %lu\n",(unsigned long)time(NULL)-(unsigned long)start_time);
	// busy waiting
	i = 0;
	while (i < 300000000) {
		i++;
	}
	
	// waiting for enough time to pass - busy waiting. i expected start_time + 4 to be enough,
	// but it seems somehow the process doesn't get resched in time to remove the second missing element
	// while (time(NULL) < start_time + LATE_TODO_PENALTY + 2 ) {
		// i++;
	// }

	finish_time = time(NULL);

	
	// printf("start time is %d\n",(int)start_time);
	// printf("finish time is %d\n",(int)finish_time);	
	
	// expecting two todos to expire
	checkDelay(start_time, finish_time, 1, "TestNumber 3");	
	
	
	
	// 5 -> 1 -> 3 -> 6 -> 2

	checkResult(read_TODO(myPid, 1, pDesc,pDeadline, pStatus) , descLength , 0, "check 1st element");	
	checkStatus (*pStatus, 9,"check 1st element");
	checkDeadline (*pDeadline, time5,"check 1st element");
	checkDesc (pDesc, str5, "check 1st element");

	checkResult(read_TODO(myPid, 2, pDesc,pDeadline, pStatus) , descLength , 0, "check 2nd element");	
	checkStatus (*pStatus, 0, "check 3rd element");
	checkDeadline (*pDeadline, time1, "check 3rd element");
	checkDesc (pDesc, str1, "check 3rd element");		
	
	checkResult(read_TODO(myPid, 3, pDesc,pDeadline, pStatus) , descLength , 0, "check 3rd element");	
	checkStatus (*pStatus, 0, "check 4th element");
	checkDeadline (*pDeadline, time3, "check 4th element");
	checkDesc (pDesc, str3, "check 4th element");		

	checkResult(read_TODO(myPid, 4, pDesc,pDeadline, pStatus) , descLength , 0, "check 4th element");	
	checkStatus (*pStatus, 0, "check 5th element");
	checkDeadline (*pDeadline, time6, "check 5th element");
	checkDesc (pDesc, str6, "check 5th element");	
	
	checkResult(read_TODO(myPid, 5, pDesc,pDeadline, pStatus) , descLength , 0, "check 5th element");	
	checkStatus (*pStatus, 0, "check 6th element");
	checkDeadline (*pDeadline, time2, "check 6th element");
	checkDesc (pDesc, str2, "check 6th element");	
	
	checkResult(read_TODO(myPid, 6, pDesc, pDeadline, pStatus) , -1 , EINVAL, "check 6th element");	
	
	
	checkResult(delete_TODO(myPid, 6) , -1 , EINVAL, "delete 6th item");	
	checkResult(delete_TODO(myPid, 5) , 0 , 0, "delete 5th item");	
	checkResult(delete_TODO(myPid, 4) , 0 , 0, "delete 4th item");	
	checkResult(delete_TODO(myPid, 3) , 0 , 0, "delete 3rd item");	
	checkResult(delete_TODO(myPid, 2) , 0 , 0, "delete 2nd item");	
	checkResult(delete_TODO(myPid, 1) , 0 , 0, "delete 1st item");	

	
	free(pStatus);
	free(pDesc);
	free(pDeadline);

	
}






void checkRemovalAndDelayOfExpiredTODOsTestNumber4 () {
	int sizeOfReadBuffer = 80;
	int descLength = 13;
	printf("--- checkRemovalAndDelayOfExpiredTODOsTestNumber4 ---\n");
	pid_t myPid = getpid();
	unsigned long i = 0;
	int* pStatus = (int*) malloc (sizeof(int));
	char* pDesc = (char*) malloc (sizeof(char)*sizeOfReadBuffer);	
	time_t* pDeadline = (time_t*) malloc (sizeof(time_t));	
	
	char str1[40] = "Description_1";
	char str2[40] = "Description_2";
	char str3[40] = "Description_3";
	char str4[40] = "Description_4";
	char str5[40] = "Description_5";
	char str6[40] = "Description_6";

	
	time_t start_time = time(NULL);
	time_t finish_time = time(NULL);
	
	time_t time1 = start_time + 3000;
	time_t time2 = start_time + 8000;
	time_t time3 = start_time + 6000;
	time_t time4 = start_time + 62;
	time_t time5 = start_time + 1;
	time_t time6 = start_time + 7000;
	
	// expected todo queue is :
	// 5 -> 4 -> 1 -> 3 -> 6 -> 2
	
		
	// add todos, different delays
	checkResult(add_TODO(myPid, str1, descLength,time1) , 0 , 0, "add1");
	checkResult(add_TODO(myPid, str2, descLength,time2) , 0 , 0, "add2");	
	checkResult(add_TODO(myPid, str3, descLength,time3) , 0 , 0, "add3");		
	checkResult(add_TODO(myPid, str4, descLength,time4) , 0 , 0, "add4");		
	checkResult(add_TODO(myPid, str5, descLength,time5) , 0 , 0, "add5");		
	checkResult(add_TODO(myPid, str6, descLength,time6) , 0 , 0, "add6");		

	printf("current time is %lu\n",(unsigned long)time(NULL)-(unsigned long)start_time);
	// busy waiting
	i = 0;
	while (i < 300000000) {
		i++;
	}

	printf("current time is %lu\n",(unsigned long)time(NULL)-(unsigned long)start_time);
	// busy waiting
	i = 0;
	while (i < 300000000) {
		i++;
	}	


	printf("current time is %lu\n",(unsigned long)time(NULL)-(unsigned long)start_time);
	// busy waiting
	i = 0;
	while (i < 300000000) {
		i++;
	}


	printf("current time is %lu\n",(unsigned long)time(NULL)-(unsigned long)start_time);
	// busy waiting
	i = 0;
	while (i < 300000000) {
		i++;
	}


	printf("current time is %lu\n",(unsigned long)time(NULL)-(unsigned long)start_time);
	// busy waiting
	i = 0;
	while (i < 300000000) {
		i++;
	}

	finish_time = time(NULL);

	
	 // printf("start time is  %d\n",(int)start_time);
	 // printf("finish time is %d\n",(int)finish_time);	
	
	// expecting two todos to expire
	checkDelay(start_time, finish_time, 2, "TestNumber 4");	
	
	
	// expected todo queue is :
	// 1 -> 3 -> 6 -> 2


	checkResult(read_TODO(myPid, 1, pDesc,pDeadline, pStatus) , descLength , 0, "check 1st element");	
	checkStatus (*pStatus, 0, "check 3rd element");
	checkDeadline (*pDeadline, time1, "check 3rd element");
	checkDesc (pDesc, str1, "check 3rd element");		
	
	checkResult(read_TODO(myPid, 2, pDesc,pDeadline, pStatus) , descLength , 0, "check 2nd element");	
	checkStatus (*pStatus, 0, "check 4th element");
	checkDeadline (*pDeadline, time3, "check 4th element");
	checkDesc (pDesc, str3, "check 4th element");		

	checkResult(read_TODO(myPid, 3, pDesc,pDeadline, pStatus) , descLength , 0, "check 3rd element");	
	checkStatus (*pStatus, 0, "check 5th element");
	checkDeadline (*pDeadline, time6, "check 5th element");
	checkDesc (pDesc, str6, "check 5th element");	
	
	checkResult(read_TODO(myPid, 4, pDesc,pDeadline, pStatus) , descLength , 0, "check 4th element");	
	checkStatus (*pStatus, 0, "check 6th element");
	checkDeadline (*pDeadline, time2, "check 6th element");
	checkDesc (pDesc, str2, "check 6th element");		

	checkResult(read_TODO(myPid, 5, pDesc, pDeadline, pStatus) , -1 , EINVAL, "check 5th element");		

	checkResult(read_TODO(myPid, 6, pDesc, pDeadline, pStatus) , -1 , EINVAL, "check 6th element");		




	
	checkResult(delete_TODO(myPid, 6) , -1 , EINVAL, "delete 6th item");	
	checkResult(delete_TODO(myPid, 5) , -1 , EINVAL, "delete 5th item");	
	checkResult(delete_TODO(myPid, 4) , 0 , 0, "delete 4th item");	
	checkResult(delete_TODO(myPid, 3) , 0 , 0, "delete 3rd item");	
	checkResult(delete_TODO(myPid, 2) , 0 , 0, "delete 2nd item");	
	checkResult(delete_TODO(myPid, 1) , 0 , 0, "delete 1st item");	

	
	free(pStatus);
	free(pDesc);
	free(pDeadline);

	
}





void forkTestNumber5 () {
	int sizeOfReadBuffer = 80;
	int descLength = 13;
	printf("--- forkTestNumber5 ---\n");

	unsigned long i = 0;
	unsigned int j = 0;
	int* pStatus = (int*) malloc (sizeof(int));
	char* pDesc = (char*) malloc (sizeof(char)*sizeOfReadBuffer);	
	time_t* pDeadline = (time_t*) malloc (sizeof(time_t));	
	
	int statusForWaitpid1, statusForWaitpid2;
	
	char str1[40] = "Description_1";
	char str2[40] = "Description_2";
	char str3[40] = "Description_3";
	char str4[40] = "Description_4";
	char str5[40] = "Description_5";
	char str6[40] = "Description_6";

	
	time_t start_time = time(NULL);
	time_t finish_time = time(NULL);
	
	time_t time1 = start_time + 3000;
	time_t time2 = start_time + 8000;
	time_t time3 = start_time + 6000;
	time_t time4 = start_time + 500;
	time_t time5 = start_time + 3;
	time_t time6 = start_time + 7000;
	
	// expected todo queue is :
	// 5 -> 4 -> 1 -> 3 -> 6 -> 2
	
	
	int sonPid1,sonPid2;
	sonPid1 = fork();
	
	if(sonPid1 == 0){               /*child1*/

		j = 0;
		while (j < 5){
			printf("son1 - current time is %lu\n",(unsigned long)time(NULL)-(unsigned long)start_time);
			// busy waiting
			i = 0;
			while (i < 300000000) {
				i++;
			}	
			j++;
		}
		exit(0);

	} else {   /*parent*/
		
		sonPid2 = fork();
		if(sonPid2 == 0) { /*child2*/
			j = 0;
			while (j < 5){
				printf("son2 - current time is %lu\n",(unsigned long)time(NULL)-(unsigned long)start_time);
				// busy waiting
				i = 0;
				while (i < 300000000) {
					i++;
				}	
				j++;
			}
			exit(0);		
			
			
		} else {  /*parent*/
			sleep(1);
			checkResult(add_TODO(sonPid1, str1, descLength,time1) , 0 , 0, "add1");
			checkResult(add_TODO(sonPid1, str2, descLength,time2) , 0 , 0, "add2");	
			checkResult(add_TODO(sonPid1, str3, descLength,time3) , 0 , 0, "add3");		
			checkResult(add_TODO(sonPid1, str4, descLength,time4) , 0 , 0, "add4");		
			checkResult(add_TODO(sonPid1, str5, descLength,time5) , 0 , 0, "add5");		
			checkResult(add_TODO(sonPid1, str6, descLength,time6) , 0 , 0, "add6");				

			checkResult(add_TODO(sonPid2, str1, descLength,time1) , 0 , 0, "add1");
			checkResult(add_TODO(sonPid2, str2, descLength,time2) , 0 , 0, "add2");	
			checkResult(add_TODO(sonPid2, str3, descLength,time3) , 0 , 0, "add3");		
			checkResult(add_TODO(sonPid2, str4, descLength,time4) , 0 , 0, "add4");		
			checkResult(add_TODO(sonPid2, str5, descLength,time5) , 0 , 0, "add5");		
			checkResult(add_TODO(sonPid2, str6, descLength,time6) , 0 , 0, "add6");	
			
			while (j < 10){
				printf("father - current time is %lu\n",(unsigned long)time(NULL)-(unsigned long)start_time);
				// busy waiting
				i = 0;
				while (i < 300000000) {
					i++;
				}	
				j++;
			}
			
			
		}
		
	}
	printf("father - waiting for sons to finish - current time is %lu\n",(unsigned long)time(NULL)-(unsigned long)start_time);
	waitpid(sonPid1, &statusForWaitpid1, 0);
	waitpid(sonPid2, &statusForWaitpid2, 0);
	printf("father - sons finished - current time is %lu\n",(unsigned long)time(NULL)-(unsigned long)start_time);

	finish_time = time(NULL);

	checkDelay(start_time, finish_time, 1, "TestNumber 5");	

	free(pStatus);
	free(pDesc);
	free(pDeadline);

	
}







int main()
{
	printf("\n");
	checkAddOrder();
	checkRemovalAndDelayOfExpiredTODOsTestNumber1();
	checkRemovalAndDelayOfExpiredTODOsTestNumber2();
	checkSkippingRemovalAndDelayOfExpiredTODOsTestNumber3();
	checkRemovalAndDelayOfExpiredTODOsTestNumber4();
	forkTestNumber5();
	
	printf("\n");

    return 0;
  
}
