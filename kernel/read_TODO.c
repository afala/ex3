/* read_TODO.c */

/*************************************************************************/

// Includes:

#include <linux/read_TODO.h>

/*************************************************************************/

// Constants:
#define SUCCESS 0
#define FAILURE -1

/*************************************************************************/

// Macros:

/*************************************************************************/

// Structs:

/*************************************************************************/

// Globals:
/*************************************************************************/

// Functions:

asmlinkage ssize_t sys_read_TODO(pid_t pid, int TODO_index, char* TODO_description, time_t *TODO_deadline, int* status)
{
	if(TODO_description == NULL || status == NULL || TODO_index < 0)
		return -EINVAL;

    task_t* p_target_task_struct;

	int res = TODO_COMMON_isLegalTask(current, pid, &p_target_task_struct);
	if (res != SUCCESS)
		return res;

	if (TODO_index > p_target_task_struct->todo_q_size)
		return -EINVAL;

    struct list_head* p_pos;
    struct list_head* p_n;
    TODO *p_todo_to_read;
    int i = 0;

    list_for_each_safe(p_pos, p_n, &(p_target_task_struct->todo_q))
    {
        if (TODO_index == ++i)
        {
            p_todo_to_read = list_entry(p_pos, TODO, queue_list);
            int todo_desc_len =  p_todo_to_read->description_size;

            //read status:
            *status = p_todo_to_read->status;
            //read description:
            if(copy_to_user(TODO_description,p_todo_to_read->description,todo_desc_len) != 0)
            	return -EFAULT;
            //read todo deadline:
            *TODO_deadline = p_todo_to_read->deadline;
            return todo_desc_len;
        }
    }

    return FAILURE;
}
