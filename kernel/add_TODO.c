/* add_TODO.c */

/*************************************************************************/

// Includes:

#include <linux/add_TODO.h>

/*************************************************************************/

// Constants:

/*************************************************************************/

// Macros:

/*************************************************************************/

// Structs:

/*************************************************************************/

// Globals:

/*************************************************************************/

// Functions:


asmlinkage int sys_add_TODO(pid_t pid, const char* TODO_description, ssize_t description_size, time_t TODO_deadline)
{
    if ((TODO_description == NULL) || (description_size < 1) || (TODO_deadline < CURRENT_TIME))
    {
        return -EINVAL;
    }
    
    
    // Get the task struct of the process of which pid is the given pid
    task_t* p_target_task_struct;
    
    // Check if it is a descendent - If it isn't, return the relevant error. else, create a TODO and add it to TODO queue of the task struct we got.
    int res = TODO_COMMON_isLegalTask(current, pid, &p_target_task_struct);
    if (res != SUCCESS)
    {
        return res;
    }
    
    // From this point on, we know that a TODO can be added to the pid given
    char *p_buffer = kmalloc(description_size*sizeof(char), GFP_KERNEL);
    if (p_buffer == NULL)
    {
        return -ENOMEM;
    }
    
    if(copy_from_user(p_buffer, TODO_description, description_size) != SUCCESS)
    {
        return -EFAULT;
    }
    
    TODO *p_todo = (TODO *)kmalloc(sizeof(TODO), GFP_KERNEL);
    if (p_todo == NULL)
    {
        return -ENOMEM;
    }
    p_todo->deadline = TODO_deadline;
    p_todo->deadline_relative = TODO_deadline - CURRENT_TIME;
    p_todo->description = p_buffer;
    p_todo->description_size = description_size;
    p_todo->status = 0;
    
    //list_add_tail(&(p_todo->queue_list),&(p_target_task_struct->todo_q));
    struct list_head *p_pos;
    TODO *p_temp_todo;
    // For empty todo queue, just add it to the beginning
    if (p_target_task_struct->todo_q_size == 0)
    {
        list_add(&(p_todo->queue_list), &(p_target_task_struct->todo_q)); 
    }
    else
    {
        list_for_each(p_pos, &(p_target_task_struct->todo_q))
        {
            p_temp_todo = list_entry(p_pos, TODO, queue_list);
            if (TODO_deadline < p_temp_todo->deadline)
            {
                list_add_tail(&(p_todo->queue_list), p_pos);
                break;
            }
            // If we are at the end of the list, add it to the end
            if (p_pos->next == (&(p_target_task_struct->todo_q)))
            {
                list_add((&p_todo->queue_list), p_pos);
                break;
            }
        }
    }
    
    p_target_task_struct->todo_q_size++;
    
    return SUCCESS;
     
}
