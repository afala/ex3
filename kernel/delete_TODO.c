/* delete_TODO.c */

/*************************************************************************/

// Includes:

#include <linux/delete_TODO.h>

/*************************************************************************/

// Constants:

/*************************************************************************/

// Macros:

/*************************************************************************/

// Structs:

/*************************************************************************/

// Globals:

/*************************************************************************/

// Functions:
asmlinkage int sys_delete_TODO(pid_t pid, int TODO_index)
{
    
    // Get the task struct of the process of which pid is the given pid
    struct task_struct* p_target_task_struct;
    
    // Check if it is a descendent - If it isn't, return FAILURE. else, create a TODO and add it to TODO queue of the task struct we got.
    int res = TODO_COMMON_isLegalTask(current, pid, &p_target_task_struct);
    if (res != SUCCESS)
    {
        return res;
    }
    
    if ((TODO_index  < 0) || (TODO_index > (p_target_task_struct->todo_q_size)))
    {
        return -EINVAL;
    }
    struct list_head* p_pos;
    struct list_head* p_n;
    TODO *p_todo_to_del;
    int i = 0;
    
    list_for_each_safe(p_pos, p_n, &(p_target_task_struct->todo_q))
    {
        p_todo_to_del = list_entry(p_pos, TODO, queue_list);
        if ((p_todo_to_del->deadline < CURRENT_TIME) && (p_todo_to_del->status == 0))
        {
            continue;
        }
        if (TODO_index == ++i)
        {
            //p_todo_to_del = list_entry(p_pos, TODO, queue_list);
            list_del(p_pos);
            kfree(p_todo_to_del->description);
            kfree(p_todo_to_del);
            p_target_task_struct->todo_q_size--;
            return SUCCESS;
        }
    }
    // If we got here, that means we didn't find the TODO_index in the queue, so something is wrong with it
    return -EINVAL;
}
