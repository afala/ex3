/* mark_TODO.c */

/*************************************************************************/

// Includes:

#include <linux/mark_TODO.h>

/*************************************************************************/

// Constants:

/*************************************************************************/

// Macros:

/*************************************************************************/

// Structs:

/*************************************************************************/

// Globals:

/*************************************************************************/

// Functions:

asmlinkage int sys_mark_TODO(pid_t pid, int TODO_index, int status)
{
    if(TODO_index<=0)
    	return -EINVAL;

    task_t* p_target_task_struct;

	int res = TODO_COMMON_isLegalTask(current, pid, &p_target_task_struct);
	if (res != SUCCESS)
		return res;

	if (TODO_index > p_target_task_struct->todo_q_size)
		return -EINVAL;

    struct list_head* p_pos;
    struct list_head* p_n;
    TODO *p_todo_to_mark;
    int i = 0;

    list_for_each_safe(p_pos, p_n, &(p_target_task_struct->todo_q))
    {
        p_todo_to_mark = list_entry(p_pos, TODO, queue_list);
        if ((p_todo_to_mark->deadline < CURRENT_TIME) && (p_todo_to_mark->status == 0))
        {
            continue;
        }
        if (TODO_index == ++i)
        {
            p_todo_to_mark->status = status; //mark status
            return SUCCESS;
        }
    }

    return FAILURE;
}
